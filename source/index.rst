======================
Koha 18.05 Manual (en)
======================

:Author: The Koha Community

.. include:: images.rst
.. toctree::
   :maxdepth: 2


   00_intro
   01_installation
   18_license

  
